<?php
namespace rp {
	
	require_once 'rpPDOStatement.php';
	require_once 'rpPDOException.php';
	
	use rp\PDOException AS rpPDOException;
	
	class PDO extends \PDO{
		
		protected string $query;
		
		public function __construct(string $dsn, string $username = NULL, string $password = NULL, array $options = NULL ){
			try{
                $this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array('\rp\PDOStatement', array($this)));
                $this->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
                $this->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
				return parent::__construct( $dsn, $username, $password, $options );
				
			}catch( \PDOException $e){
				throw new rpPDOException( '000-000-04D', sprintf( 'Unexpected error while creating rpPDO instance, with msg "%s"', $e->getMessage() ) );
			}
		}
		
		/**
		 * @param string $query
		 * @param array $options
		 * @return PDOStatement|false
         */
		public function prepare( $query, $options = array()){
			$this->query = $query;
			return parent::prepare($query, $options);
		}
	} //end class
} //end namespace


