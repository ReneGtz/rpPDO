<?php

namespace rp {
	
	use PDO;
	use rp\PDOException AS rpPDOException;
	
	require_once 'rpPDOException.php';
	
	class PDOStatement extends \PDOStatement {
	 
		/** @var array holds all parameters assigned to the statement via "bindParam()" or "bindValue()" */
		private array $parameters = array();
		
		/** @var callable|null if set this function will execute on an successfully execute() command. The returned value will be passed to from the execute function */
		private $successFunction = null;
		
		/** @var callable|null if set, this function will execute if an error occurs. The returned value will returned by execute */
		private $errorFunction = null;
		
		/** this variable seams to be needed for PDO to work */
		private PDO $pdo;
		
		/**
		 * it's seams that pdo need this function to work
		 * rpPDOStatement constructor.
		 * @param PDO $pdo */
		protected function __construct(PDO $pdo){
			$this->pdo = $pdo;
		}
		
		/**
		 * returns the array of binding Parameters fof this statement
		 * @return array */
		public function getBindParameters() : array {
			return $this->parameters;
		} //end function getBindParameters()
		
		private ?array $definition = null;
		
		/**
		 * declares the format, how the result array will be returned
		 *
		 *
		 * @param array $definition an array where the keys must match the keys of the result and the values represents the new key.<br/>
		 * <b>Example:</b> <br/>
		 * SELECT `id`, `name`, `group` FROM `accounts`;<br/>
		 * setResultFormat( array( <br/>
		 * ..'id' => 'accountID', <br/>
		 * ..'name' => 'accountName',<br/>
		 * ..'group' => accountGroup'<br/>
		 * ) );<br/>
		 * 
		 * The returned result would be array(<br/>
		 *      'accountID' => 17,<br/>
		 *      'accountName' => 'Merlin',<br/>
		 *      'accountGroup' => 3<br/>
		 * );<br/>
		 * <br/>
		 * <b>Note</b>: if an success-function is set with "setOnSuccess()" this flag is ignored and the result of the success-function will be returned.
		 */
		public function setResultFormat( array $definition ) :void {
			$this->definition = $definition;
		} //end function setResultFormat()
		
		/**
		 * if $definition is set, the function will reformat $result 
		 * 
		 * @param array $result the result to reform
		 * @return array|rpPDOException <b>array</b> the result with the applied format. The Input if no definition is set.<br/><b>null</b> if an error occurred
		 */
		private function applyDefinitionFormat( array $result ) :array|rpPDOException {
			if( $this->definition === null ){
				return $result;
			}
			$toRet = array();
			foreach($result as $row) {
				$toRetRow = &$toRet[];
				foreach( $this->definition AS $oldKey => $newKey ){
					if( !array_key_exists( $oldKey, $row ) ){
					    return new rpPDOException( '000-000-00G', sprintf( 'Failed to apply definition on result. Missing key "%s" in result', $oldKey) );
					}
					$toRetRow[ $newKey ] = $row[ $oldKey ];
				} //end foreach column
			} //end foreach row
			return $toRet;
		} //end function applyDefinitionFormat()
		
		private bool $stmIsClosed;
        
        /** execute the statement
         * @param null $params
         * @param bool $closeCursor set to false, if you dont want to reset the statement parameter. ( For example if you want to execute the statement again with similar parameter. )
         * @return array|int|mixed|rpPDOException <b>array</b> if OnSuccess or OnError is not set. ( The fetched result of the query )<br/>
         * <b>int</b> if result-mode is set to RESULT_MODE_ROW_COUNT or RESULT_MODE_INSERTED_ID <br/>
         * <b>mixed</b> if OnSuccess or OnError is set. ( Return the result from the compounding function )
         * @internal To set OnSuccess or OnError use the functions "setOnSuccess()" or "setOnError()".<br/>
         */
		public function execute( $params = null, bool $closeCursor = true ) :mixed {
			try {
				$this->stmIsClosed = false;
				$result = parent::execute($params);
			} catch(PDOException $e){
				
			    if( $this->resultMode === self::RESULT_MODE_NONE ){
			        throw new $e;
                }
				//if errorFunction is set
				if( is_callable( $this->errorFunction ) ){
					$error_result = call_user_func($this->errorFunction, $e, $this);
					
					// if result is not null, the exception is handled
					if($error_result !== null){
						return $error_result;
					}
				} //end if errorFunction is set
				
                throw new rpPDOException( '000-000-00H', sprintf( 'Execution of query "%s" failed because of an exception "%s".', $this->queryString, $e->getMessage() ) );
			} //end try catch
            
			try{
				if($this->successFunction !== null){
				    $preparedResult = call_user_func($this->successFunction, $result, $this);
				    if( $this->resultMode === self::RESULT_MODE_FETCH ){
				        return $this->applyDefinitionFormat( $preparedResult );
                    }
					return $preparedResult;
				}
				
				switch( $this->resultMode ){
                    case self::RESULT_MODE_NONE: //do nothing special. behave like normal PDO
                        return $result;
				    
					case self::RESULT_MODE_FETCH:
						$data = $this->fetchAll();
						if( $data === null ){
						    throw new rpPDOException( '000-000-00I', sprintf( 'Failed to fetch result from query "%s"', $this->queryString ) );
						}
						$data = $this->applyDefinitionFormat( $data );
						break;
					
					case self::RESULT_MODE_ROW_COUNT:
						$data = $this->rowCount();
						break;
						
                    case self::RESULT_MODE_INSERTED_ID:
                        $data = $this->pdo->lastInsertId();
                        break;
					
					default:
					    throw new rpPDOException( '000-000-03F', sprintf( 'Unknown result mode "%s" on execute.', $this->resultMode ) );
				}
				
				return $data;
			} finally {
				// if cursor is not already closed - close it
				if( $this->stmIsClosed === false && $closeCursor === true && $this->resultMode !== self::RESULT_MODE_NONE ){
					$this->closeCursor();
				}
			} //end try finally 
		} //end function execute()
		
		/** Set the function to be called if the statement was successfully executed
		 * @param callable $function the function to be called
		 * @internal The passed function will get the result from the PDO::execute() as first parameter and the statement as second parameter<br/>
		 * 			The returned value of the passed function will be returned by execute() <br/>
		 * 			Note: please take the second parameter as reverence.<br/>
         *          If result mode is set to FETCH, the definition format will be tried to be applied */
		public function setOnSuccess(callable $function){
			$this->successFunction = $function;
		} //end function setOnSuccess()
		
		/** Set the function to be called if the statement end up in an PDOException
		 * @param callable $function the function to be called
		 * @internal The passed function will get the occurred error as first parameter and the statement as second parameter<br/>
		 * 			If the passed function return anything other than "NULL" it's will be interpreted as "the PDOException was successfully handled" and the returned value will be passed back to the execute() <br/>
		 * 			If the passed function return "NULL" it's will be interpreted as "the PDOException was not handled". In this case <b>rpPDOException</b> will be returned<br/>
		 * 			Note: please take the second parameter as reverence. */
		public function setOnError(callable  $function){
			$this->errorFunction = $function;
		} //end function setOnError();
		
		
		/**
		 * @param mixed $param
		 * @param mixed $var
		 * @param int $type
		 * @param null $maxLength
		 * @param null $driverOptions
		 * @return bool
		 */
		public function bindParam( $param, &$var, $type = PDO::PARAM_INT, $maxLength = NULL, $driverOptions = NULL) :bool {
            $this->parameters[$param] = $var;
			return parent::bindParam($param, $var, $type, $maxLength, $driverOptions);
		} //end function bindParam()
		
		/**
		 * @param mixed $param
		 * @param mixed $value
		 * @param int $type
		 * @return bool
		 */
		public function bindValue( $param, $value, $type = PDO::PARAM_INT) :bool {
            $this->parameters[$param] = $value;
			return parent::bindValue($param, $value, $type);
		} //end function bindValue()
		
		/**
		 * Closes the cursor, enabling the statement to be executed again.
		 * 
		 * @param bool $reset if <b>true</b> (default) all bind parameters will reset.<br/>if set to <b>false</b> the bind parameter will stay and can be reused for the next query
		 * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure
		 */
		public function closeCursor(bool $reset = true) :bool
        {
			if($reset === true){
				foreach($this->parameters AS $key => $val){
					parent::bindValue($key, NULL, PDO::PARAM_NULL);
				}
				$this->parameters = array();
			}
			$toRet = parent::closeCursor();
			$this->stmIsClosed = true;
			return $toRet;
		} //end function closeCursor()
		
        const RESULT_MODE_NONE = 0;
		const RESULT_MODE_FETCH = 1;
		const RESULT_MODE_ROW_COUNT = 2;
		const RESULT_MODE_INSERTED_ID = 3;
		
		private int $resultMode = self::RESULT_MODE_FETCH;
		
		/**
		 * set the information, that should be returned by self::execute()<br/>
		 * <br/>
         * <b>RESULT_MODE_NONE</b> = execute() will behave like the original PDO<br/>
		 * <b>RESULT_MODE_FETCH</b> = execute() will return the result of fetch()<br/>
		 * <b>RESULT_MODE_ROW_COUNT</b> = execute() will return the result of rowCount()<br/>
         * <b>RESULT_MODE_INSERTED_ID</b> = execute() will return the last inserted ID<br/>
		 * <br/> 
		 * <b>Note:</b>: if an own success-function was set with "setOnSuccess()", this flag will be ignored and the result of the success-function will be returned.
		 * 
		 * @param int $mode the value of an RESULT_MODE_* constant
		 * @return bool|rpPDOException <b>true</b> on success.<br/><b>rpPDOException</b> if an error occurred.
		 */
		public function setResultMode( int $mode ) : bool|rpPDOException {
            $this->resultMode = match ( $mode ) {
                self::RESULT_MODE_NONE,
                self::RESULT_MODE_FETCH,
                self::RESULT_MODE_ROW_COUNT,
                self::RESULT_MODE_INSERTED_ID => $mode,
                default => throw new rpPDOException( '000-000-00J', sprintf( 'Unknown result-mode "%s".', $mode ) ),
            }; //end switch mode
			return true;
		} //end function setResultMode
		
	} //end class
} //end namespace