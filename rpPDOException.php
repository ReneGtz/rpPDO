<?php

namespace rp;

use rp\error AS rpError;
use rp\exception AS rpException;

class PDOException extends \PDOException {
    
    private string $errorID;
    
    private string $logMsg;
    
    private string $pubMsg;
    
    private int $httpCode;
    
    private ?self $previous;
    
    /**
     * PDOException constructor.
     * @param string $errorID
     * @param string $logMsg
     * @param string $pubMsg
     * @param int $httpStatusCode
     * @param PDOException|null $previous
     */
    public function __construct( string $errorID, string $logMsg, string $pubMsg = 'Internal Error', int $httpStatusCode = 500, self $previous = null){
        $this->errorID = $errorID;
        $this->logMsg = $logMsg;
        $this->pubMsg = $pubMsg;
        $this->httpCode = $httpStatusCode;
        $this->previous = $previous;
        parent::__construct($pubMsg, 0, $previous);
    }
    
    /**
     * @return string <b>string</b>
     */
    public function getErrorID() : string {
        return $this->errorID;
    }
    
    /**
     * @return string <b>string</b>
     */
    public function getLogMsg() : string {
        return $this->logMsg;
    }
    
    /**
     * @return string <b>string</b>
     */
    public function getPubMsg() : string {
        return $this->pubMsg;
    }
    
    /**
     * @return int <b>int</b>
     */
    public function getHttpCode() :int {
        return $this->httpCode;
    }
    
    /**
     * @return PDOException|null <b>rp\PDOException</b> if any previous is set<br/><b>null</b> otherwise
     */
    public function getPreviousError() : ?PDOException {
        return $this->previous;
    }
    
    /**
     * @param string $logMsg
     * @param string|null $pubMsg
     * @param int|null $httpStatusCode
     * @return $this <b>rp\PDOException</b>
     */
    public function extend( string $logMsg, ?string $pubMsg, ?int $httpStatusCode = null ) :self {
        $this->previous = new self( $this->getErrorID(), $this->getLogMsg(), $this->getPubMsg(), $this->getHttpCode(), $this->getPreviousError() );
        $this->logMsg = $logMsg;
        if( $pubMsg !== null ){
            $this->pubMsg = $pubMsg;
        }
        if( $httpStatusCode !== null ){
            $this->httpCode = $httpStatusCode;
        }
        
        return $this;
    }
    
    /**
     * @return rpError <b>rp\error</b>
     */
    public function getAsError() :rpError {
        return $this->_getAsError( $this );
    }
    
    /**
     * @param PDOException $error
     * @return rpError <b>rp\error</b>
     */
    private function _getAsError( self $error ) :rpError {
        if( $error->getPreviousError() !== null ){
            $newPrevious = $this->_getAsError( $error->getPreviousError() );
        } else {
            $newPrevious = null;
        }
        return new rpError( $this->getErrorID(), $this->getLogMsg(), $this->getPubMsg(), $this->getHttpCode(), $newPrevious );
    }
    
    /**
     * @return rpException <b>rp\exception</b>
     */
    public function getAsException() :rpException {
        return $this->_getAsException( $this );
    }
    
    /**
     * @param PDOException $error
     * @return rpException <b>rp\exception</b>
     */
    private function _getAsException( self $error ) :rpException {
        if( $error->getPreviousError() !== null ){
            $newPrevious = $this->_getAsException( $this->getPreviousError() );
        } else {
            $newPrevious = null;
        }
        return new rpException( $this->getErrorID(), $this->getLogMsg(), $this->getPubMsg(), $this->getHttpCode(), $newPrevious );
    }
    
    /**
     * @param string $logMsg
     * @param string|null $pubMsg
     * @param int|null $httpStatusCode
     * @return rpError <b>rp\error</b>
     */
    public function extendAsError( string $logMsg, ?string $pubMsg, ?int $httpStatusCode = null ) :rpError {
        $this->extend( $logMsg, $pubMsg, $httpStatusCode );
        return $this->getAsError();
    }
    
    /**
     * @param string $logMsg
     * @param string|null $pubMsg
     * @param int|null $httpStatusCode
     * @return rpException <b>rp\exception</b>
     */
    public function extendAsException( string $logMsg, ?string $pubMsg, ?int $httpStatusCode = null ) :rpException {
        $this->extend( $logMsg, $pubMsg, $httpStatusCode );
        return $this->getAsException();
    }
    
} //end class